from isiguardianlib.isolation.const import ISOLATION_CONSTANTS
ISOLATION_CONSTANTS['ALL_DOF'] = ['X', 'Y', 'Z', 'RX', 'RY','RZ']
ISOLATION_CONSTANTS['DOF_LISTS'] = (['Z', 'RX', 'RY'], ['X', 'Y','RZ'])
#ISOLATION_CONSTANTS['CART_BIAS_DOF_LISTS'] = ([], [])
from isiguardianlib.HPI import *
prefix = 'HPI-HAM1'
request = 'ROBUST_ISOLATED'
